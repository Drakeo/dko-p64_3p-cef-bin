#!/usr/bin/env bash

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
exec 4>&1; export BASH_XTRACEFD=4; set -x

# make errors fatal
set -e
# bleat on references to undefined shell variables
set -u

# Check autobuild is around or fail
if [ -z "$AUTOBUILD" ] ; then
    exit 1
fi
if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

top="$(pwd)"
stage="$(pwd)/stage"

# Load autobuild provided shell functions and variables
source_environment_tempfile="$stage/source_environment.sh"
"$AUTOBUILD" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

# version number to build  (this is what to change to updatge CEF build)
# it will get propagated into the URLs below
CEF_PACKAGE_VERSION="3626.1895.g7001d56"

# URLs of the CEF bundle to download
CEF_BUNDLE_URL_WINDOWS32="https://secondlife-cef-builds.s3.amazonaws.com/cef_binary_3."$CEF_PACKAGE_VERSION"_windows32.tar.bz2"
CEF_BUNDLE_URL_WINDOWS64="https://secondlife-cef-builds.s3.amazonaws.com/cef_binary_3."$CEF_PACKAGE_VERSION"_windows64.tar.bz2"
CEF_BUNDLE_URL_DARWIN64="https://secondlife-cef-builds.s3.amazonaws.com/cef_binary_3."$CEF_PACKAGE_VERSION"_macosx64.tar.bz2"
CEF_BUNDLE_URL_LINUX="http://opensource.spotify.com/cefbuilds/cef_binary_3."$CEF_PACKAGE_VERSION"_linux${AUTOBUILD_ADDRSIZE}_minimal.tar.bz2"

# file where the CEF bundle will be downloaded to before unpacking etc.
CEF_BUNDLE_DOWNLOAD_FILE_WINDOWS="${top}/stage/windows${AUTOBUILD_ADDRSIZE}.bz2"
CEF_BUNDLE_DOWNLOAD_FILE_DARWIN64="${top}/stage/darwin64.bz2"
CEF_BUNDLE_DOWNLOAD_FILE_LINUX="${top}/stage/linux${AUTOBUILD_ADDRSIZE}.bz2"

# directories where the downloaded, unpacked, modified and ready to build CEF
# bundle will end up and where it will be built by Cmake
CEF_BUNDLE_SRC_DIR_WINDOWS="${top}/stage/windows${AUTOBUILD_ADDRSIZE}"
CEF_BUNDLE_SRC_DIR_DARWIN64="${top}/stage/darwin64"
CEF_BUNDLE_SRC_DIR_LINUX="${top}/stage/linux${AUTOBUILD_ADDRSIZE}"

# used in VERSION.txt but common to all bit-widths and platforms
build=${AUTOBUILD_BUILD_ID:=0}

case "$AUTOBUILD_PLATFORM" in
    windows*)
        # download bundle
        CEF_BUNDLE_URL="CEF_BUNDLE_URL_WINDOWS${AUTOBUILD_ADDRSIZE}"
        curl "${!CEF_BUNDLE_URL}" -o "${CEF_BUNDLE_DOWNLOAD_FILE_WINDOWS}"

        # Create directory for it and untar, stripping off the complex CEF name
        mkdir -p "${CEF_BUNDLE_SRC_DIR_WINDOWS}"
        tar xvfj "${CEF_BUNDLE_DOWNLOAD_FILE_WINDOWS}" -C "${CEF_BUNDLE_SRC_DIR_WINDOWS}" --strip-components=1

        # replace the Visual Studio C++ Run Time setting to ones that matches viewer
        # Note: we don't build/use the debug version but it's included here for completeness
        sed -i -e 's/\/MTd/\/MDd/g' "${CEF_BUNDLE_SRC_DIR_WINDOWS}/cmake/cef_variables.cmake"
        sed -i -e 's/\/MT/\/MD/g' "${CEF_BUNDLE_SRC_DIR_WINDOWS}/cmake/cef_variables.cmake"

        # create solution file cef.sln in build folder
        cd "${CEF_BUNDLE_SRC_DIR_WINDOWS}"
        rm -rf build
        mkdir -p build
        cd build
        cmake -G "$AUTOBUILD_WIN_CMAKE_GEN" ..

        # build release version of wrapper only
        build_sln cef.sln "Release|$AUTOBUILD_WIN_VSPLATFORM" "libcef_dll_wrapper"

        # create folders to stage files in
        mkdir -p "$stage/bin/release"
        mkdir -p "$stage/include/cef/include"
        mkdir -p "$stage/lib/release"
        mkdir -p "$stage/resources"
        mkdir -p "$stage/LICENSES"

        # binary files
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/chrome_elf.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/d3dcompiler_43.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/d3dcompiler_47.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/libcef.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/libEGL.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/libGLESv2.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/natives_blob.bin" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/snapshot_blob.bin" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/v8_context_snapshot.bin" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/widevinecdmadapter.dll" "$stage/bin/release/"

        # include files
        cp -r "${CEF_BUNDLE_SRC_DIR_WINDOWS}/include/." "$stage/include/cef/include/"

        # resource files
        cp -r "${CEF_BUNDLE_SRC_DIR_WINDOWS}/Resources/" "$stage/"

        # library files
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/libcef.lib" "$stage/lib/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/build/libcef_dll_wrapper/Release/libcef_dll_wrapper.lib" "$stage/lib/release/"

        # license file
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/LICENSE.txt" "$stage/LICENSES/cef.txt"

        # populate version_file (after header files are copied to a well specified place that version.cpp can access)
        cl \
            /Fo"$(cygpath -w "$stage/version.obj")" \
            /Fe"$(cygpath -w "$stage/version.exe")" \
            /I "$(cygpath -w "$stage/include/cef/include")"  \
            /I "$(cygpath -w "$stage/include/cef")"  \
            /D "AUTOBUILD_BUILD=${build}" \
            "$(cygpath -w "$top/version.cpp")"
        "$stage/version.exe" > "$stage/version.txt"
        rm "$stage"/version.{obj,exe}
    ;;

    darwin64)
        # download bundle
        CEF_BUNDLE_URL="CEF_BUNDLE_URL_DARWIN64"
        curl "${!CEF_BUNDLE_URL}" -o "${CEF_BUNDLE_DOWNLOAD_FILE_DARWIN64}"

        # Create directory for it and untar, stripping off the complex CEF name
        mkdir -p "${CEF_BUNDLE_SRC_DIR_DARWIN64}"
        tar xvfj "${CEF_BUNDLE_DOWNLOAD_FILE_DARWIN64}" -C "${CEF_BUNDLE_SRC_DIR_DARWIN64}" --strip-components=1

        # build version number and write to autobuild version file
        VERSION_HEADER_FILE="$CEF_BUNDLE_SRC_DIR_DARWIN64/include/cef_version.h"
        version=$(sed -n -E 's/#define CEF_VERSION "([a-z0-9.]+)"/\1/p' "${VERSION_HEADER_FILE}")
        echo "${version}.${build}" > "${stage}/VERSION.txt"

        BUILD_FOLDER="build"
        cd "${CEF_BUNDLE_SRC_DIR_DARWIN64}"
        rm -rf "${BUILD_FOLDER}"
        mkdir -p "${BUILD_FOLDER}"
        cd "${BUILD_FOLDER}"

        cmake -G "Xcode" -DPROJECT_ARCH="x86_64" ..

        xcodebuild -project cef.xcodeproj -target libcef_dll_wrapper -configuration Release

        # create folders to stage files in
        mkdir -p "$stage/bin/release"
        mkdir -p "$stage/include/cef/include"
        mkdir -p "$stage/lib/release"
        mkdir -p "$stage/LICENSES"

        # include files
        cp -r "${CEF_BUNDLE_SRC_DIR_DARWIN64}/include/." "$stage/include/cef/include/"

        # library file
        cp "${CEF_BUNDLE_SRC_DIR_DARWIN64}/${BUILD_FOLDER}/libcef_dll_wrapper/Release/libcef_dll_wrapper.a" "$stage/lib/release/"

        # framework
        cp -r "${CEF_BUNDLE_SRC_DIR_DARWIN64}/Release/Chromium Embedded Framework.framework" "$stage/bin/release/"

        # include files
        cp -r "${CEF_BUNDLE_SRC_DIR_DARWIN64}/include/." "$stage/include/cef/include/"

        # license file
        cp "${CEF_BUNDLE_SRC_DIR_DARWIN64}/LICENSE.txt" "$stage/LICENSES/cef.txt"
    ;;

    linux*)
        # download bundle
        CEF_BUNDLE_URL="${CEF_BUNDLE_URL_LINUX}"
        curl "${CEF_BUNDLE_URL}" -o "${CEF_BUNDLE_DOWNLOAD_FILE_LINUX}"

        # Create directory for it and untar, stripping off the complex CEF name
        mkdir -p "${CEF_BUNDLE_SRC_DIR_LINUX}"
        tar xvfj "${CEF_BUNDLE_DOWNLOAD_FILE_LINUX}" -C "${CEF_BUNDLE_SRC_DIR_LINUX}" --strip-components=1

        VERSION_HEADER_FILE="$CEF_BUNDLE_SRC_DIR_LINUX/include/cef_version.h"
        version=$(sed -n -E 's/#define CEF_VERSION "([a-z0-9.]+)"/\1/p' "${VERSION_HEADER_FILE}")
        echo "${version}.${build}" > "${stage}/VERSION.txt"

        cd "${CEF_BUNDLE_SRC_DIR_LINUX}"
        rm -rf build
        mkdir -p build
        cd build

        cmake .. -DCMAKE_CXX_FLAGS=-m${AUTOBUILD_ADDRSIZE} || true
        make -j6 libcef_dll_wrapper
        cd ../..

        mkdir -p "${stage}/include/cef/include"
        mkdir -p "${stage}/lib/release"
        mkdir -p "${stage}/resources"
        mkdir -p "${stage}/LICENSES"

        cp ${CEF_BUNDLE_SRC_DIR_LINUX}/build/libcef_dll_wrapper/libcef_dll_wrapper.a ${stage}/lib/release/

        cp -R ${CEF_BUNDLE_SRC_DIR_LINUX}/Release/* ${stage}/lib/release/
        cp -R ${CEF_BUNDLE_SRC_DIR_LINUX}/Resources/* ${stage}/resources/

        cp -R ${CEF_BUNDLE_SRC_DIR_LINUX}/include/* ${stage}/include/

        cp "${CEF_BUNDLE_SRC_DIR_LINUX}/LICENSE.txt" "$stage/LICENSES/cef.txt"
    ;;

esac
