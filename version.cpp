/**
 * @file   version.cpp
 * @author Callum Prentice via Nat Goodspeed
 * @date   2017-02-13
 * @brief  Report library version number - C++ version
 *         For a library whose version number is tracked in a C++ header
 *         file, it's more robust to build a helper program to report it than
 *         to manually parse the header file. The library might change the
 *         syntax with which it defines the version number, but we can assume
 *         it will remain valid C / C++.
 * 
 * $LicenseInfo:firstyear=2014&license=internal$
 * Copyright (c) 2017, Linden Research, Inc.
 * $/LicenseInfo$
 */

#include "cef_version.h"
#include <iostream>

int main(int argc, char* argv[])
{
    std::cout << CEF_VERSION << "." << AUTOBUILD_BUILD;
    return 0;
}

